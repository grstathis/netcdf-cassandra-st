/**
 * 
 */
package gr.demokritos.iit.netcdf.cassandra;

import java.io.IOException;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;

/**
 * @author Yiannis Mouchakis
 *
 */
public class Main {
	
	/**
	 * prints help message
	 * @param options the available options
	 */
	private static void help(Options options) {
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("<application name>", options, true);
	}

	/**
	 * @param args
	 * @throws IOException 
	 * @throws QueryEvaluationException 
	 * @throws MalformedQueryException 
	 * @throws RepositoryException 
	 */
	public static void main(String[] args) throws IOException, RepositoryException, MalformedQueryException, QueryEvaluationException {
		
		//start options
		Options start_options = new Options();
		
		OptionGroup start_group = new OptionGroup();
		start_group.setRequired(true);
	
		Option help = Option.builder("h")
				.desc("prints help message")
				.longOpt("help")
				.required()
				.build();
		start_group.addOption(help);
		
		Option o_import = Option.builder("i")
				.desc("import netcdf header in cassandra")
				.longOpt("import")
				.required()
				.build();
		start_group.addOption(o_import);
		
		Option search = Option.builder("s")
				.desc("search netcdf metadata with a term and get datasets containing it")
				.longOpt("search")
				.required()
				.build();
		start_group.addOption(search);
		
		start_options.addOptionGroup(start_group);
		
		
		//search options
		Options search_options = new Options();
		
		search_options.addOption(search);
		
		Option endpoint = Option.builder("e")
				.desc("endpoint to be used")
				.longOpt("endpoint")
				.numberOfArgs(1)
				.required()
				.build();
		search_options.addOption(endpoint);
		
		Option term = Option.builder("t")
				.desc("term to search")
				.longOpt("term")
				.numberOfArgs(1)
				.required()
				.build();
		search_options.addOption(term);
		
		//import options
		Options import_options = new Options();
		
		import_options.addOption(o_import);
		
		Option address = Option.builder("a")
				.desc("cassandra address")
				.longOpt("address")
				.numberOfArgs(1)
				.required()
				.build();
		import_options.addOption(address);

		Option port = Option.builder("p")
				.desc("cassandra port")
				.longOpt("port")
				.numberOfArgs(1)
				.type(Integer.class)
				.required()
				.build();
		import_options.addOption(port);

		Option file = Option.builder("f")
				.desc("netcdf file locations if you want to import files to cassandra. "
						+ "if no files are provided then this will only create the keyspace and the tables")
				.longOpt("file")
				.hasArgs()
				.build();
		import_options.addOption(file);
		
		Option keyspace = Option.builder("k")
				.desc("cassandra keyspace to be used. by default \"netcdf_headers\"")
				.longOpt("keyspace")
				.numberOfArgs(1)
				.build();
		import_options.addOption(keyspace);
		
		Option replication = Option.builder("r")
				.desc("replication factor to be used in keyspace. by default 1")
				.longOpt("replication")
				.numberOfArgs(1)
				.type(Integer.class)
				.build();
		import_options.addOption(replication);
		
		//begin parsing
		CommandLineParser parser = new DefaultParser();
			
		
		CommandLine start_cmdl = null;
		try {
			start_cmdl = parser.parse(start_options, args, true);
		} catch (ParseException e) {
			System.err.println("Parsing failed, correct usage is:");
			help(start_options);
			throw new IllegalArgumentException(e);
		}
		
		if (start_cmdl.hasOption(help.getOpt())) {
			
			help(start_options);
			
		}
		else if (start_cmdl.hasOption(o_import.getOpt())) {
			
			CommandLine import_cmdl = null;
			try {
				import_cmdl = parser.parse(import_options, args);
			} catch (ParseException e) {
				System.err.println("Parsing failed, correct usage is:");
				help(import_options);
				throw new IllegalArgumentException(e);
			}
			
			NetCDFToCassandra nc_to_cass = new NetCDFToCassandra(
					import_cmdl.getOptionValue(address.getOpt()),
					new Integer(import_cmdl.getOptionValue(port.getOpt()))
					);
			
			if ( import_cmdl.hasOption(keyspace.getOpt() ) ) {
				nc_to_cass.setKeyspace(keyspace.getValue());
			}
			
			if ( import_cmdl.hasOption(replication.getOpt() ) ) {
				nc_to_cass.setReplicationFactor(new Integer(replication.getValue()));
			}
			
			nc_to_cass.connect();
			
			if (import_cmdl.hasOption(file.getOpt())) {				
				for (String ncfile : import_cmdl.getOptionValues(file.getOpt())) {
					nc_to_cass.export(ncfile);
				}
			}
			
			nc_to_cass.close();
		} 
		else if (start_cmdl.hasOption(search.getOpt())) {
			
			CommandLine search_cmdl = null;
			try {
				search_cmdl = parser.parse(search_options, args);
			} catch (ParseException e) {
				System.err.println("Parsing failed, correct usage is:");
				help(search_options);
				throw new IllegalArgumentException(e);
			}
			
			SemagrowToNetCDF sema_serch = new SemagrowToNetCDF(search_cmdl.getOptionValue(endpoint.getOpt()));
			Set<String> datasets = sema_serch.termSearch(search_cmdl.getOptionValue(term.getOpt()));
			for (String dataset : datasets) {
				System.out.println(dataset);
			}
			
		}


	}

}
